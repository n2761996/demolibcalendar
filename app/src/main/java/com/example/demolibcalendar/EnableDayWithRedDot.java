package com.example.demolibcalendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import androidx.core.content.ContextCompat;

import com.example.calendarlib.CalendarDay;
import com.example.calendarlib.DayViewDecorator;
import com.example.calendarlib.DayViewFacade;
import com.example.calendarlib.spans.DotSpan;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public class EnableDayWithRedDot implements DayViewDecorator {

    private Context context;
    private HashSet<CalendarDay> dates;
    private boolean isToday;

    public EnableDayWithRedDot(Context context,
                               Collection<CalendarDay> dates,
                               boolean isToday) {
        this.context = context;
        this.dates = new HashSet<>(dates);
        this.isToday = isToday;
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }
    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void decorate(DayViewFacade view) {
        view.setDaysDisabled(false);

        LayerDrawable layerDrawable =
                (LayerDrawable) context.getResources().getDrawable(R.drawable.bg_day_view);
        if (!isToday) {
            GradientDrawable gradientDrawable =
                    (GradientDrawable) layerDrawable.findDrawableByLayerId(R.id.bg_item_day_view);
            gradientDrawable.setStroke(0, Color.WHITE);
        }
        view.setBackgroundDrawable(layerDrawable);
        view.addSpan(new DotSpan(4, context.getResources().getColor(R.color.red_dot)));
    }
}
