package com.example.demolibcalendar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.example.calendarlib.CalendarDay;
import com.example.calendarlib.MaterialCalendarView;
import com.example.calendarlib.OnDateSelectedListener;

import org.threeten.bp.LocalDate;
import org.threeten.bp.Month;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    MaterialCalendarView calendarView;
    public LocalDate min;
    public LocalDate max;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        min = LocalDate.of(LocalDate.now().getYear() - 1, Month.JANUARY, 1);
        max = LocalDate.of(LocalDate.now().getYear(), Month.DECEMBER, 31);

        calendarView = (MaterialCalendarView) findViewById(R.id.view_calendar);

        calendarView.state()
                .edit()
                .setMinimumDate(min)
                .setMaximumDate(max)
                .commit();

        calendarView.addDecorator(new DisableDecorator());
        drawCalendar();
    }

    private void drawCalendar() {
        calendarView.addDecorator(new EnableDayWithRedDot(this, getRedDates(), false));
        calendarView.addDecorator(new EnableDayWithBlueDot(this, getBlueDates(), false));
        calendarView.addDecorator(new EnableDayWithTwoDots(this, getTwoDotsDates(), true));
    }

    private List<CalendarDay> getRedDates() {
        List<CalendarDay> redDates = new ArrayList<>();
        redDates.add(CalendarDay.from(LocalDate.now().minusDays(5)));
        redDates.add(CalendarDay.from(LocalDate.now().minusDays(3)));
        redDates.add(CalendarDay.from(LocalDate.now().minusDays(1)));
        return redDates;
    }

    private List<CalendarDay> getBlueDates() {
        List<CalendarDay> redDates = new ArrayList<>();
        redDates.add(CalendarDay.from(LocalDate.now().minusDays(4)));
        redDates.add(CalendarDay.from(LocalDate.now().minusDays(2)));
        return redDates;
    }

    private List<CalendarDay> getTwoDotsDates() {
        List<CalendarDay> redDates = new ArrayList<>();
        redDates.add(CalendarDay.from(LocalDate.now()));
        return redDates;
    }


}