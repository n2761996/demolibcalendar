package com.example.demolibcalendar;

import com.example.calendarlib.CalendarDay;
import com.example.calendarlib.DayViewDecorator;
import com.example.calendarlib.DayViewFacade;

public class DisableDecorator implements DayViewDecorator {

    public DisableDecorator() {
    }

    @Override
    public boolean shouldDecorate(CalendarDay calendarDay) {
        return true;
    }

    @Override
    public void decorate(DayViewFacade dayViewFacade) {
        dayViewFacade.setDaysDisabled(true);
    }
}
